/**
 * Created by marthamareal on 7/19/17.
 */

import java.util.regex.Matcher
import java.io.File

/**
 * Created by marthamareal on 7/18/17.
 */
class Arranged {

    static void main(String[] args) {
        List<String> save2 = new ArrayList<>()
        List<String> save = new ArrayList<>()
        def date = null
        def time = null
        def sender = ""
        def deli_status = ""
        def reciever = ""
        def type_name = ""
        def sequence = ""
        def command_status = null
        def dateRegex = /^\d{4}-\d{2}-\d+/
        def timeRegex = /\d{2}:\d{2}:\d{2}/
        def message = ""
        def allmessages = ""

        println "DATE        " + "\t\t" + "|TIME        " + "\t\t" + "|COMMAND_STATUS" + "\t\t" + "|SEQUENCE_NO       " + "\t\t" + "|SENDER        " + "\t\t" + "|RECIEVER      " + "\t\t" + "|MASSAGE       " + "\t\t"

        new File('src/bearerbox_server.log').eachLine { line ->

            if (line.contains("bind_transmitter_resp") || line.contains("enquire_link_resp")) {
                //println "You can start"
                deli_status += "delivered"
            }
            if (line.contains("submit_sm_resp") || line.contains(" deliver_sm_resp")) {
                // println "Delivered"
                deli_status += "delivered"

            }

            if (line.endsWith('dump:')) {

                Matcher matchdate = line =~ dateRegex
                Matcher matchtime = line =~ timeRegex
                if (matchdate.find() && matchtime.find())
                    date = matchdate.group().toString()
                time = matchtime.group().toString()
            } else if (line.contains("data:") && !line.contains("PDU dump ends")) {
                message += line.substring(99)
            } else if (line.contains("source_addr")) {
                sender = line.substring(54, line.length() - 1)
            } else if (line.contains("destination_addr")) {
                reciever = line.substring(59, line.length() - 1)
            } else if (line.contains("command_status")) {
                command_status = line.substring(56)
            } else if (line.contains("type_name: ")) {
                type_name = line.substring(51);
            } else if (line.contains("sequence_number: ")) {
                sequence = line.substring(57);
            } else if (line.contains("type_name: ")) {
                type_name = line.substring(51);
            } else if (line.contains("sequence_number: ")) {
                sequence = line.substring(57);
            } else if (line.contains("PDU dump ends")) {



                deli_status += date + "     " + time + "     " + command_status + "     " + sender + "      " + reciever + "     " + type_name + "     " + sequence + "     " + message + "  "
               // println deli_status
                save.add(deli_status)

                deli_status = ""
                message = ""
                sender = ""
                reciever = ""
                command_status=""


            }



        }

        save.retainAll {
            !it.contains("delivered")
        }
        // println save

        save.each {
            println it
        }

    }


}
